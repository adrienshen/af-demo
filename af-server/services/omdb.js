const request = require('request-promise')
const apikey = '65de49a9'

function fetch(movieQuery) {
    return request.get(`http://www.omdbapi.com/?apikey=${apikey}&t=${movieQuery}`)
}

module.exports = {
    fetch: fetch,
}