let sfJson = require('../data/sf_movie_locations.json')

function queryAutoComplete(partialSearchString, cb) {
  let matches = _mapFilterTitles(sfJson, partialSearchString)
  console.log('matchesArr: ', matches)
  cb(matches)
}

function _mapFilterTitles(sfJson, partialSearchString) {
  return sfJson.filter((movie, idx) => {
      let movieTitle = movie.title.toLowerCase()
      return movieTitle.includes(partialSearchString)
  })
  .map(movie => movie.title)
  .filter((value, idx, self) => self.indexOf(value) === idx)
}

function filmLocation(movieSearchString, cb) {
    let matches = _getMovies(sfJson, movieSearchString)
    cb(matches)
}

function _getMovies(sfJson, movieSearchString) {
    return sfJson.filter((movie, idx) => {
        return movie.title === movieSearchString
    })
}

module.exports = {
    queryAutoComplete: queryAutoComplete,
    filmLocation: filmLocation,
    // Export private methods for unit testing
    privateMethods: {
        _mapFilterTitles: _mapFilterTitles,
        _getMovies: _getMovies,

    }
}