var express = require('express');
var router = express.Router();
var mfilms = require('../models/films');
var somdb = require('../services/omdb');

router.get('/autocomplete', (req, res) => {
  if (!req.query.partial) {
    return res.json({ message: "Missing required parameters" })
  }
  let partialSearchString = req.query.partial.toLowerCase()
  mfilms.queryAutoComplete(partialSearchString, results => {
    res.send({
        noMatches: results.length,
        suggestions: results,
    })
  })
})

router.get('/films', function(req, res) {
  if (!req.query.movie) {
    return res.json({ message: "Missing required parameters" })
  }
  let movieSearchString = req.query.movie
  mfilms.filmLocation(movieSearchString, function(results) {
    somdb.fetch(movieSearchString).then(resp => {
      console.log('resp: ', resp)
      res.send({
        movies: results,
        omdb: JSON.parse(resp),
      })    
    }).catch(error => {
      console.error('Error: ', error)
      res.send({
        movies: results,
        omdb: null,
      })
    })
  })
})

module.exports = router