'use strict';

const chai = require('chai')
const expect = require('chai').expect
chai.use(require('chai-http'))

const mfilms = require('../../models/films')
const moviesJson = require('../../data/sf_movie_locations.json')

describe('/models/films functions', function() {
    this.timeout(5000)

    before(function() {
        
    })

    after(function() {
        
    })

    it('_mapFilterTitles returns correct results', function() {
        let results = mfilms.privateMethods._mapFilterTitles(moviesJson, "blue")
        let results2 = mfilms.privateMethods._mapFilterTitles(moviesJson, "jasm")

        expect(results[0]).to.equal("Blue Jasmine")
        expect(results2[0]).to.equal("Blue Jasmine")
    })

    it('_getMovies return correct results', function() {
        let results = mfilms.privateMethods._getMovies(moviesJson, "Blue Jasmine")
        let results2 = mfilms.privateMethods._getMovies(moviesJson, "Ant-Man")

        expect(results[0]["locations"]).to.equal("5546 Geary Ave")
        expect(results.length).to.equal(28)
        expect(results2[0]["locations"]).to.equal("California between Kearney and Davis")
        expect(results2.length).to.equal(13)
    })
})
