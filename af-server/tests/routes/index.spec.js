'use strict';

const chai = require('chai')
const expect = require('chai').expect
chai.use(require('chai-http'))

const api = require('../../app.js')

/* Integration tests */
describe('API endpoint /autocomplete', function() {
    this.timeout(5000)

    before(function() {

    })

    after(function() {

    })

    it('should return autocomplete suggestion', function() {
        return chai.request(api)
            .get('/api/autocomplete')
            .query({partial: 'blue'})
            .then(response => {
                expect(response).to.have.status(200)
                expect(response).to.be.json
                expect(response.body).to.be.an('object')
                expect(response.body.suggestions).to.be.an('array')
            })
    })

    it('should return error if params not correct', function() {
        return chai.request(api)
            .get('/api/autocomplete')
            .query({incorrect: 'incorrect'})
            .then(response => {
                missingParameterExpectations(response)
            })
    })
})

describe('API endpoint /films', function() {
    this.timeout(5000)

    before(function() {

    })

    after(function() {

    })

    it('return correct movies results', function() {
        let filmQuery = 'Blue Jasmine'
        return chai.request(api)
            .get('/api/films')
            .query({movie: filmQuery})
            .then(response => {
                expect(response).to.have.status(200)
                expect(response.body.movies[0].title).to.be.equal(filmQuery)
                expect(response.body.movies).to.be.an('array')
                expect(response.body.movies.length).to.be.equal(28)
            })
    })

    it('return error on wrong parameters', function() {
        return chai.request(api)
            .get('/api/films')
            .query({incorrect: 'incorrect'})
            .then(response => {
                console.log('response: ', response.body)
                missingParameterExpectations(response)
            })
    })
})

function missingParameterExpectations(response) {
    expect(response).to.have.status(200)
    expect(response.body.message).to.be.an('string')
    expect(response.body.message).to.be.equal('Missing required parameters')
}