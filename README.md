## Challenge
```
SF Movies

Create a service that shows on a map where movies have been filmed in San Francisco. The user should be able to filter the view using autocompletion search.

The data is available on DataSF: Film Locations.
```

## Solution
[Live Demo, SF Movies](https://secret-citadel-67141.herokuapp.com/)
[Source Code](https://gitlab.com/adrienshen/af-demo)


Using Heroku and Node(Express), we create 2 api endpoints. One to provide auto-complete suggestions when user types, and the other one to fetch the correct movies and movie descript from an open source movie database. The data is provided in JSON format from https://data.sfgov.org/Culture-and-Recreation/Film-Locations-in-San-Francisco/yitu-d5am

On the client, we visualize the service as a map of San Fransisco that provides a input for users to type into. We leverage the use of MapBox GL and Google Maps Geocoding APIs for this. [Create React App](https://github.com/facebookincubator/create-react-app) is used to bootstrap the client app.

## Start the Application
### Folder Organization
The project is organized into `af-client` and `af-server` codebases. The `/functions` and firebase was originally used for the backed, but then migrated over to a stand-alone node express backend. It is left kept for review purposes.

### Clone Project
```
git clone https://gitlab.com/adrienshen/af-demo.git
```
### Fetch Dependencies
```
cd af-client && yarn install
cd af-server && npm install
```
### Start Express Server
Server will start on port 3000, or port 5000 if using `heroku local`
```
cd af-server
npm start
// Or if using heroku
heroku local
```
### Start React Client
React application proxies api requests to port 5000, this can be changed in package.json
```
cd af-client
yarn start
```
## Deployment
The client first needs to be built using react-scripts webpack. It will be copied over into `server-af/build`
```
//in  af-client
yarn build
```
The the package is ready to deploy to the clouds, in my case, Heroku.
```
// in af-server
git add build/
git commit -m 'new build'
git push heroku master
```
## Areas for Review
The main server routes are in `af-server/routes/index.js`. There are only 2 routes neccesary to implement the features.

Unit test are in `af-server/tests` seperated into models and routes. Endpoint testing in `tests/routes/index.spec.js`. It's not complete test coverage, just enough to make sure things work. I usually add more tests as I develop an application further and see where things tend to break.

On the client, `af-client/src/App.js` is the entry point for the react app. Almost everything is a component, and pure components are prefer over extending the Component class. Styling is created as components using Styled-Components. There are more than 1 components in a file, and I think that is generally okay for small apps. There is only one exported component per file.

All state changes and api requests are contained `src/mutations`. See below for in-depth state management.

## Json Data
For this project. A json file `af-server/data/sf_movie_locations.json` is used for the data store. I am aware that json file is not production scalable, but should be fine with less than 10 users. MongoDB was considered, but due to Heroku and time constraints was not implemented. In addition Json is very fast way to proto-type something.

## Difficulties
The most challenging part of this problem is finding out the geo-coordinates from very fuzzy location names. First I try using the MapBox API services for this, but the data returns were not accurate enough. Finally, I decided to use the Google Map Geocoding API which gave more accurate results.

The other difficulty was test React Components. The MapBox libraries have a conflict with the JSDom renderer. I have not fix this yet, but instead did some basic smoke testing of individual components.

## To Dos
If I have more time, I would improve the `location text` to `geocoding`. By analyzing the patterns of the location text, eg: Marina Blvd from Laguna to Baker  and then experimenting with google's api. Certain location strings can be transformed before sending to the gmaps api. In the UI, we can visualize a geometric polygon(bbox) that covers an area instead of a single point.

I would also move the json data file to a database such as mongodb. I did not use mongodb here because heroku requires credit card and verification to use add-ons.

More testing of React UI.

## Unit Testing
For nodejs backend
```
// in af-server
npm run test
```
For react client
```
// in af-client
yarn test
```

## More Indepth on Tech Choices
### Nodejs
The file organization is typical of small Express apps, but is not a really a MVC. I used express because it is lightweight and easy to setup. It's also mature and proven for micro apis. Originally it was implemented in Firebase Cloud Functions, which is also based on Express, so the code was very easy to migrate. 

`request-promise` is used for for network calls from node.

`chia mocha chia-http` used for unit and integration testing.

### Client UI
Clientside to use React.js bootstraping it with Create React App.

For the UI, I've opted to go for a mobile-focused view as I normally prefer to do with web-apps. ~80% of internet traffic in Asia is centered on mobile.

#### Styled Components
I believe CSSinJS is the more productive way to style React components. This allows the developer to think of styling as just components like everything else in React. My go to library for this is Style Components.

#### JS State Management
Due to the size of the app - very small, keeping component setState() is fine. To keep better organization, we will keep state changes in separate functions using functional setState() which is supported natively by the React API. If the application gets any bigger or have more developers, then implementing Redux is a next good step.