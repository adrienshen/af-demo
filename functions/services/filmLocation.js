
function filmLocation(req, res) {
    let movieSearchString = req.query.movie
    let sfJson = require('../data/sf_movie_locations.json')
    let matches = getMovies(sfJson, movieSearchString)

    res.send({
        movies: matches,
    })
}

function getMovies(sfJson, movieSearchString) {
    return sfJson.filter((movie, idx) => {
        return movie.title === movieSearchString
    })
}

module.exports = filmLocation