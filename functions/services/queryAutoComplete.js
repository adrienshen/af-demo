function queryAutoComplete(req, res) {
    let partialSearchString = req.query.partial.toLowerCase()
    let sfJson = require('../data/sf_movie_locations.json')
    let matches = mapFilterTitles(sfJson, partialSearchString)

    console.log('matchesArr: ', matches)
    res.send({
        noMatches: matches.length,
        suggestions: matches,
    })
}

function mapFilterTitles(sfJson, partialSearchString) {
    return sfJson.filter((movie, idx) => {
        let movieTitle = movie.title.toLowerCase()
        return movieTitle.includes(partialSearchString)
    })
    .map(movie => movie.title)
    .filter((value, idx, self) => self.indexOf(value) === idx)
}

module.exports = queryAutoComplete
