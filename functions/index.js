const functions = require('firebase-functions')
const cors = require('cors')({origin: true})

const filmLocation = require('./services/filmLocation')
const queryAutoComplete = require('./services/queryAutoComplete')

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

exports.film = functions.https.onRequest((req, res) => {
    cors(req, res, () => filmLocation(req, res))
})

exports.autoComplete = functions.https.onRequest((req, res) => {
    cors(req, res, () => queryAutoComplete(req, res))
})
