import React from 'react'
import styled from 'styled-components'

const SearchResultsPanel = props => {
  const { handleFilmSearch, autocompletes, searchCondition,
    fetchingRequest } = props
  let displayAutoComplete = (searchCondition === "autocomplete")
    && !fetchingRequest
    && autocompletes.length
  return (
    <section>
      <SearchPanelBox searchCondition={searchCondition==="results"}>
        <SearchInput
          handleInputCancel={props.handleInputCancel}
          searchQueryInput={props.searchQueryInput}
          handleSearchInputType={props.handleSearchInputType} />
        {displayAutoComplete ?
          <AutoCompleteContainerDiv>
            <AutoCompleteUl>
              {autocompletes.map((suggestion, idx) => {
                if (idx < 6) {
                  return <AutoCompleteLi
                            onClick={() => handleFilmSearch(suggestion)}
                            key={idx}>
                    <AutoCompleteEntry>{suggestion}</AutoCompleteEntry>
                  </AutoCompleteLi>
                } else {
                  return null
                }
              })}
            </AutoCompleteUl>
          </AutoCompleteContainerDiv> : null
        }
      </SearchPanelBox>
    </section>
  )
}

const SearchInput = props => {
  return (
    <SearchContainer>
      <SearchIconSvg />
      <SearchInputStyled
        value={props.searchQueryInput}
        onChange={(e) => props.handleSearchInputType(e)}
        placeholder="Search movies, actors, places" type="text" id="q" name="_q" />
      <CancelIcon
        onClick={props.handleInputCancel}>x</CancelIcon>
    </SearchContainer>
  )
}

const SearchIconSvg = props => (
    <SearchIcon fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
        <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
        <path d="M0 0h24v24H0z" fill="none"/>
    </SearchIcon>
)

const SearchPanelBox = styled.div`
    position: absolute;
    top: ${props => props.searchCondition ? '500px' : '250px'};
    left: 5px;
    width: 95%;
    height: 50px;
    border: 2px solid #ccc;
    border-radius: 5px;
    background-color: #fff;
    opacity: .90;
    transition: top .3s linear;
`
const SearchContainer = styled.div`
  height: inherit;
  position: relative;
  border-bottom: 1px solid #222;
`
const SearchIcon = styled.svg`
  display: block;
  position: absolute;
  top: 15px;
  left: 10px;
`
const CancelIcon = styled.span`
  position: absolute;
  top: 12px;
  right: 15px;
  font-size: 1.4rem;
  color: #909090;
`
const SearchInputStyled = styled.input`
  box-sizing: border-box;
  font-size: 12px;
  width: 100%;
  border: 0;
  background-color: transparent;
  height: inherit;
  margin: 0;
  color: rgba(0,0,0,.5);
  padding: 10px 10px 10px 40px;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  outline: none;
`
const AutoCompleteContainerDiv = styled.div`
  background: #fff;
  width: 100%;
`
const AutoCompleteUl = styled.ul`
  margin: 0;
  list-style: none;
  padding: 0;
`
const AutoCompleteLi = styled.li`
  border-bottom: 2px solid #ddd;
`
const AutoCompleteEntry = styled.div`
  padding: 12px;
  font-weight: 500;
  font-size: .9rem;
`

export default SearchResultsPanel