import React from 'react'

const MoviesMapView = props => {
  return (
    <div style={{
      position: 'absolute',
      top: 55,
      bottom: 0,
      width: '100%',
    }} ref={props.mapContainer} />
  )
}

export default MoviesMapView