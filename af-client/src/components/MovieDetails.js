import React from 'react'
import styled from 'styled-components'

import Dialog from 'material-ui/Dialog'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import Typography from 'material-ui/Typography'
import CloseIcon from 'material-ui-icons/Close'
import Slide from 'material-ui/transitions/Slide'

const MovieDetails = ({toggleMovieDetail, movieDetails, movieDetailsVisible, searchCondition}) => {
  return (
    <MovieDetailOverlay>
      <MoviePosterClickable
        movieDetails={movieDetails}
        toggleMovieDetail={toggleMovieDetail} />
      <MovieAdditionalInformation
        movieDetails={movieDetails}
        toggleMovieDetail={toggleMovieDetail}
        movieDetailsVisible={movieDetailsVisible} />
    </MovieDetailOverlay>
  )
}

const MoviePosterClickable = props => {
  return (
      <PosterImg
        onClick={() => props.toggleMovieDetail()}
        src={props.movieDetails.Poster}
        alt="search movie poster" />
  )
}

const MovieAdditionalInformation = ({toggleMovieDetail, movieDetailsVisible, movieDetails}) => {
  return (
    <Dialog
      fullScreen
      open={movieDetailsVisible}
      onRequestClose={() => toggleMovieDetail()}
      transition={Transition}
    >
      <AppBar style={{position: 'relative'}}>
          <Toolbar>
          <IconButton color="contrast"
            onClick={() => toggleMovieDetail()}
              aria-label="Close">
              <CloseIcon />
          </IconButton>
          <Typography type="title" color="inherit" style={{flex: 1}}>
            {movieDetails.Title}
          </Typography>
          </Toolbar>
      </AppBar>
      <DialogSection>
        <DialogDetailItem>
          <DetailLabel>Actors:</DetailLabel>
          {movieDetails.Actors}
        </DialogDetailItem>
        <DialogDetailItem>
          <DetailLabel>Plot:</DetailLabel>
          {movieDetails.Plot}
        </DialogDetailItem>
        <DialogDetailItem>
          <DetailLabel>Website:</DetailLabel>
          {movieDetails.Website}
        </DialogDetailItem>
        <DialogDetailItem>
          <DetailLabel>imdb Rating:</DetailLabel>
          {movieDetails.imdbRating}
        </DialogDetailItem>
      </DialogSection>
    </Dialog>
  )
}

const Transition = props => {
  return <Slide direction="up" {...props} />
}

const MovieDetailOverlay = styled.section`
  position: absolute;
  width: 70px;
  top: 20;
  left: 0;
  height: 20vh;
`
const PosterImg = styled.img`
  width: 70px;
  margin-left: 16px;
  cursor: pointer;
`
const DialogSection = styled.section`
  padding: 12px;
  color: #555;
`
const DialogDetailItem = styled.div`
  margin-bottom: 16px;
`
const DetailLabel = styled.div`
  font-weight: 600;
  font-size: .9rem;
`

export default MovieDetails