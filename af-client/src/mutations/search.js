import axios from 'axios'
import { AUTOCOMPLETE_ENDPOINT, FILM_ENDPOINT, GMAPS_GEOCODE_ENDPOINT } from '../endpoints'

export function searchAutoCompleteSuccess(autocompletes) {
    return (state, props) => {
        return {
            fetchingRequest: false,
            searchCondition: 'autocomplete',
            autocompletes: autocompletes
        }
    }
}

export function searchAutoCompleteError(error) {
    return (state, props) => {
        return {
            fetchingRequest: false,
            searchCondition: 'ready',
            errorState: 'NO_SUGGESTIONS',
            autocomplete: [],
        }
    }
}

export function filmSearchSuccess(jsonData) {
    return (state, props) => {
        console.log('jsonData ', jsonData)
        return {
            searchCondition: 'results',
            fetchingRequest: false,
            movieLocations: jsonData.movies.map(movie => {
                return {
                    title: movie.title,
                    locations: movie.locations,        
                }
            }),
            movieDetails: jsonData.omdb,
            errorState: false,
            error: null,
        }
    }
}

export function filmSearchError(error) {
    return (state, props) => {
        return {
            fetchingRequest: false,
            movieLocations: [],
            errorState: "FILMS_API",
            error: error,
        }
    }
}

export function resetSearchStates(state, props) {
    return {
        fetchingRequest: true,
        autocompletes: [],
    }
}

export function resetInput(state, props) {
    return {
        searchQueryInput: '',
        searchCondition: 'ready',
        movieLocations: [],
        errorState: false,
        error: null,
    }
}

export function setTypeValue(typedValue) {
    return function(state, props) {
        return {
            searchQueryInput: typedValue,
        }
    }
}

export function autoCompleteRequest(queryTyped) {
    let endpoint = AUTOCOMPLETE_ENDPOINT.replace('$string', queryTyped)
    return axios.get(endpoint)
}

export function filmSearchRequest(filmQuery) {
    let endpoint = FILM_ENDPOINT.replace('$string', filmQuery)
    return axios.get(endpoint)
}

export function requestGeoCoding(movies, callback) {
    let promises = []
    movies.map((elem, idx) => {
        let query = `${elem.locations} san fransisco`
        let endpoint = GMAPS_GEOCODE_ENDPOINT
            .replace('$address', query.replace(/ /g, '+'))
            .replace('+&+', '+and+')
        // console.log('gmaps query: ', endpoint)
        promises.push(
            axios.get(endpoint)
        )
        return false
    })

    axios.all(promises).then(resp => {
        let clean = resp.map(elem => {
            return {...elem.data.results[0]}
        })
        .filter(elem => elem.geometry)
        callback(clean)
    })
}
