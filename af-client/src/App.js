import React, { Component } from 'react'
import styled from 'styled-components'
import debounce from 'lodash/debounce'
import './App.css'
import {
  resetSearchStates,
  setTypeValue,
  autoCompleteRequest,
  searchAutoCompleteSuccess,
  searchAutoCompleteError,
  filmSearchRequest,
  filmSearchError,
  filmSearchSuccess,
  requestGeoCoding,
  resetInput,
} from './mutations/search'

import SearchResultsPanel from './components/SearchResultsPanel'
import MoviesMapView from './components/MoviesMapView'
import MovieDetails from './components/MovieDetails'

import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js'
mapboxgl.accessToken = 'pk.eyJ1IjoiYWRyaWVuc2hlbiIsImEiOiJjamF1bnlzMnY1ZTlkMzFucW41ZXFnYXNvIn0.d_O6hEinTD_7_NpuWEwZ0g'

class App extends Component {
  render() {
    return (
      <div className="App">
        <AppHeader>
          <AppTitle>SF Movie Locations</AppTitle>
        </AppHeader>
        <MoviesMapContainer />
      </div>
    );
  }
}

class MoviesMapContainer extends Component {
  constructor() {
    super()
    this.state = {
      searchQueryInput: '',
      searchCondition: 'ready',
      /* ready, autocomplete, results */
      autocompletes: [],
      movieLocations: [],
      fetchingRequest: false,
      errorState: false,
      error: null,
      movieDetails: {},
      movieDetailsVisible: false,
    }
  }

  componentDidMount() {
    let sfCoordinates = [-122.4312, 37.7739]
    let defaultZoomLevel = 10
  
    this.map = new mapboxgl.Map({
      container: this.mapContainer,
      style: 'mapbox://styles/mapbox/streets-v9',
      center: sfCoordinates,
      zoom: defaultZoomLevel,
      pitch: 60,
    })
    this.map.addControl(new mapboxgl.NavigationControl())
    this.map.on('load', () => {
      this.addPointsLayer()
      this.addMouseEvents()
    })
  }

  addMoviePins() {
    this.map.removeLayer('points')
    this.map.removeSource('points')
    requestGeoCoding(this.state.movieLocations, results => {
      // console.log('in callback, cleaned results ', results)
      let geoJson = results.map((elem, idx) => {
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [
              elem.geometry.location.lng,
              elem.geometry.location.lat
            ]
          },
          "properties": {
            "title": this.state.searchQueryInput,
            "icon": "harbor",
            "description": elem.formatted_address,
          }
        }
      })
      this.addPointsLayer(geoJson)
    })
  }

  addPointsLayer(pointsData=[]) {
    this.map.addLayer({
      "id": "points",
      "type": "symbol",
      "source": {
          "type": "geojson",
          "data": {
              "type": "FeatureCollection",
              "features": pointsData
          }
      },
      "layout": {
          "icon-image": "{icon}-15",
          "text-field": "{title}",
          "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
          "text-offset": [0, 0.6],
          "text-anchor": "top"
      }
    })
  }

  addMouseEvents() {
    this.map.on('click', 'points', (e) => {
      this.map.flyTo({center: e.features[0].geometry.coordinates})
      new mapboxgl.Popup()
        .setLngLat(e.features[0].geometry.coordinates)
        .setHTML(e.features[0].properties.description)
        .addTo(this.map)
    })
  }

  componentWillUnmount() {
    this.map.remove()
  }

  handleSearchInputType(e) {
    let typedValue = e.target.value
    this.setState(setTypeValue(typedValue))

    if (typedValue.length > 2 && typedValue.length < 30) {
      this.setState(resetSearchStates)
      this.handleSearchAutoComplete(typedValue)
    }
  }

  handleSearchAutoComplete = debounce(typedValue => {
    autoCompleteRequest(typedValue).then(resp => {
      this.setState(searchAutoCompleteSuccess(resp.data.suggestions))
    }).catch(err => {
      this.setState(searchAutoCompleteError(err))
    })
  }, 1000)

  handleFilmSearch(filmQuery) {
    this.setState(resetSearchStates)
    this.setState(setTypeValue(filmQuery))
    filmSearchRequest(filmQuery).then(resp => {
      this.setState(filmSearchSuccess(resp.data), this.addMoviePins)
    }).catch(err => {
      this.setState(filmSearchError(err))
    })
  }

  handleInputCancel() {
    this.setState(resetInput)
  }

  toggleMovieDetail() {
    this.setState({
      movieDetailsVisible: !this.state.movieDetailsVisible,
    })
  }

  render() {
    return <MapBoxContainer>
      <MoviesMapView
        mapContainer={el => this.mapContainer = el} />
      {this.state.searchCondition==="results" ?
        <MovieDetails {...this.state}
          toggleMovieDetail={this.toggleMovieDetail.bind(this)} /> : null
      }
      <SearchResultsPanel {...this.state}
        handleInputCancel={this.handleInputCancel.bind(this)}
        handleFilmSearch={this.handleFilmSearch.bind(this)}
        handleSearchInputType={this.handleSearchInputType.bind(this)} />
    </MapBoxContainer>
  }
}

const MapBoxContainer = styled.div`
  width: 100%;
`
const AppHeader = styled.div`
  background-color: #222;
  opacity: .8;
  height: 30px;
  padding: 20px;
  color: white;
`
const AppTitle = styled.div`
  font-size: 1rem;
`

export default App