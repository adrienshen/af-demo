import React from 'react'
import ReactDOM from 'react-dom'
import MovieDetails from './components/MovieDetails'
import SearchResultsPanel from './components/SearchResultsPanel'
import MoviesMapView from './components/MoviesMapView'

const mstate = {
  searchQueryInput: '',
  searchCondition: 'ready',
  autocompletes: [],
  movieLocations: [],
  fetchingRequest: false,
  errorState: false,
  error: null,
  movieDetails: {},
  movieDetailsVisible: false,
}

it('renders <MovieDetails /> without crashing', () => {
  const div = document.createElement('div')
  const movieDetails = {
    "Actors": "A, B, C",
    "Poster": "https://images-na.ssl-images-amazon.com/images/M/MV5BMTc0ODk5MzEyMV5BMl5BanBnXkFtZTcwMzI0MDY1OQ@@._V1_SX300.jpg",
    "Website": "test.website.com",
    "imdbRating": 9.0,
  }
  ReactDOM.render(
    <MovieDetails
      movieDetails={movieDetails}
      movieDetailsVisible={true}
      searchCondition="results"
    />, div)
})

it('renders <SearchResultsPanel /> without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <SearchResultsPanel
      {...mstate}
      handleInputCancel={alert()}
      handleFilmSearch={alert()}
      handleSearchInputType={alert()}
    />, div)
})

it('renders <MoviesMapView /> without crashing', () => {
  const div = document.createElement('div')
  let map = new Object()
  ReactDOM.render(
    <MoviesMapView
      mapContainer={el => map.mapContainer = el}/>,
    div
  )
})